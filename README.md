### Install Service

brew install postgres
psql

psql=# create database reputation;
psql=# create user reputation with encrypted password 'reputation';
psql=# grant all privileges on database reputation to reputation;


### Install Project

git clone git@bitbucket.org:opendream/reputation.git
cd reputation
mkvirtualenv --python=`which python3` reputation
pip install -r requirements.txt
python manage.py migrate


### Crawler
scrapy crawl ncc_case_settled -o results/ncc_case_settled.json
scrapy crawl deka_search -o results/deka_search.json
scrapy crawl sec_enforce -o results/sec_enforce.json
scrapy crawl sec_person_position_in_org -o results/sec_person_position_in_org.json