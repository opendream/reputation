import re


def get_names(full_name):

    full_name = full_name.strip()

    sname = full_name.split(' ')

    last_name_index = -1
    try:
        if sname[-2] == 'ณ':
            last_name_index = -3
    except IndexError:
        pass

    last_name = sname[last_name_index]


    first_name = ' '.join(sname[0:last_name_index])
    prefix_name = ''

    if len(first_name.split(' ')) > 1:
        first_name = first_name.split(' ')[-1]
        prefix_name = ' '.join(first_name.split(' ')[0:-1])

    if len(first_name.split('.')) > 1:
        first_name = first_name.split('.')[-1]
        prefix_name = '.'.join(first_name.split('.')[0:-1]) + '.'


    if first_name[0:6] in ['นางสาว']:
        prefix_name = first_name[0:6]
        first_name = first_name[6:]

    elif first_name[0:3] in ['นาย', 'นาง']:
        prefix_name = first_name[0:3]
        first_name = first_name[3:]

    elif first_name[0:2] in ['คุณ']:
        prefix_name = first_name[0:2]
        first_name = first_name[2:]

    to_replace_list = 'ร้อยตรี ร้อยเอก เรือตรี-เรือเอก เรืออากาศตรี เรืออากาศเอก พันตรี นาวาตรี นาวาอากาศตรี พันโท นาวาโท นาวาอากาศโท พันเอก นาวาเอก นาวาอากาศเอก พันเอกพิเศษ นาวาเอกพิเศษ นาวาอากาศเอกพิเศษ พลตรี พลเรือตรี พลอากาศตรี พลโท พลเรือโท พลอากาศโท พลเอก พลเรือเอก พลอากาศเอก พลตำรวจ สิบตำรวจโท สิบตำรวจตรี สิบตำรวจเอก จ่าสิบตำรวจ จ่าสิบตำรวจพิเศษ ดาบตำรวจ ร้อยตำรวจเอก พันตำรวจตรี พันตำรวจโท พันตำรวจเอก พันตำรวจเอกพิเศษ พลตำรวจตรี พลตำรวจโท พลตำรวจเอก'.split(' ')
    for i in to_replace_list:
        first_name = first_name.replace(i, '')

    last_name = re.sub(r'[0-9/]*\s*$', '', last_name)

    return {
        'prefix_name': prefix_name,
        'first_name': first_name,
        'last_name': last_name,
        'full_name': full_name,
    }