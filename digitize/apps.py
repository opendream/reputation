from django.apps import AppConfig


class DigitizeConfig(AppConfig):
    name = 'digitize'
