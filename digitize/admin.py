from django.contrib import admin

from .models import Person, Organization, Project, Case, \
    OrganizationPerson, OrganizationCase, OrganizationProject, \
    ProjectCase, ProjectPerson, CasePerson

admin.site.register(Person)
admin.site.register(Organization)
admin.site.register(Project)
admin.site.register(Case)

admin.site.register(OrganizationPerson)
admin.site.register(OrganizationCase)
admin.site.register(OrganizationProject)
admin.site.register(ProjectCase)
admin.site.register(ProjectPerson)
admin.site.register(CasePerson)

