import graphene
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType, ObjectType

from common.utils import clear_name_title
from .models import Person, Organization, Project, Case,\
    OrganizationPerson, OrganizationCase, OrganizationProject, \
    ProjectCase, ProjectPerson, CasePerson


class OrganizationPersonType(DjangoObjectType):
    class Meta:
        model = OrganizationPerson
        interfaces = (relay.Node, )


class OrganizationCaseType(DjangoObjectType):
    class Meta:
        model = OrganizationCase
        interfaces = (relay.Node,)


class OrganizationProjectType(DjangoObjectType):
    class Meta:
        model = OrganizationProject
        interfaces = (relay.Node,)


class ProjectCaseType(DjangoObjectType):
    class Meta:
        model = ProjectCase
        interfaces = (relay.Node,)


class ProjectPersonType(DjangoObjectType):
    class Meta:
        model = ProjectPerson
        interfaces = (relay.Node,)


class CasePersonType(DjangoObjectType):
    person_judgement = graphene.Boolean()
    class Meta:
        model = CasePerson
        interfaces = (relay.Node,)


class PersonType(DjangoObjectType):
    organizations = graphene.List(OrganizationPersonType)
    projects = graphene.List(ProjectPersonType)
    cases = graphene.List(CasePersonType)

    @graphene.resolve_only_args
    def resolve_organizations(self):
        return self.person_organizations.all()

    @graphene.resolve_only_args
    def resolve_projects(self):
        return self.person_projects.all()

    @graphene.resolve_only_args
    def resolve_cases(self):
        return self.person_cases.all()

    class Meta:
        model = Person
        filter_fields = {
            'id': ['exact'],
            'first_name': ['exact', 'in'],
            'last_name': ['exact', 'in'],
        }

        interfaces = (relay.Node, )


class OrganizationType(DjangoObjectType):
    organization_type = graphene.String()

    people = graphene.List(OrganizationPersonType)
    projects = graphene.List(OrganizationProjectType)
    cases = graphene.List(OrganizationCaseType)

    @graphene.resolve_only_args
    def resolve_people(self):
        return self.organization_people.all()

    @graphene.resolve_only_args
    def resolve_projects(self):
        return self.organization_projects.all()

    @graphene.resolve_only_args
    def resolve_cases(self):
        return self.organization_cases.all()

    class Meta:
        model = Organization
        filter_fields = {
            'id': ['exact'],
            'name': ['exact', 'in'],
            'organization_type': ['exact', 'in'],
        }
        interfaces = (relay.Node, )


class ProjectType(DjangoObjectType):
    people = graphene.List(ProjectPersonType)
    organizations = graphene.List(OrganizationProjectType)
    cases = graphene.List(ProjectCaseType)

    @graphene.resolve_only_args
    def resolve_people(self):
        return self.project_people.all()

    @graphene.resolve_only_args
    def resolve_organizations(self):
        return self.project_organizations.all()

    @graphene.resolve_only_args
    def resolve_cases(self):
        return self.project_cases.all()

    class Meta:
        model = Project
        filter_fields = {
            'id': ['exact'],
            'name': ['exact', 'in'],
            'address_province': ['exact', 'in'],
            'address_district': ['exact', 'in'],
            'address_sub_district': ['exact', 'in'],
        }

        interfaces = (relay.Node, )


class CaseType(DjangoObjectType):
    people = graphene.List(CasePersonType)
    organizations = graphene.List(OrganizationCaseType)
    projects = graphene.List(ProjectCaseType)

    @graphene.resolve_only_args
    def resolve_people(self):
        return self.case_people.all()

    @graphene.resolve_only_args
    def resolve_organizations(self):
        return self.case_organizations.all()

    @graphene.resolve_only_args
    def resolve_projects(self):
        return self.case_projects.all()

    class Meta:
        model = Case
        filter_fields = {
            'id': ['exact'],
            'cause_of_case': ['exact', 'in'],
        }
        interfaces = (relay.Node, )


class Query(ObjectType):

    person = relay.Node.Field(PersonType)
    person_list = DjangoFilterConnectionField(PersonType)

    organization = relay.Node.Field(OrganizationType)
    organization_list = DjangoFilterConnectionField(OrganizationType)

    case = relay.Node.Field(CaseType)
    case_list = DjangoFilterConnectionField(CaseType)


class EventMutation(relay.ClientIDMutation):
    class Input:
        person_id = graphene.Int()
        fullname = graphene.String()

        organization_id = graphene.Int()
        organization_name = graphene.String()
        organization_type = graphene.String()

        position = graphene.String()
        position_year = graphene.Decimal()
        position_start_year = graphene.Decimal()
        position_end_year = graphene.Decimal()

        case_id = graphene.Int()
        cause_of_case = graphene.String()
        black_case_number = graphene.String()
        red_case_number = graphene.String()

        address_province = graphene.String()
        address_district = graphene.String()
        address_sub_district = graphene.String()

        case_status = graphene.String()
        case_summary = graphene.String()

        is_guilty = graphene.Boolean()
        guilty_level = graphene.String()
        person_judgement = graphene.String()

    success = graphene.Boolean()

    person_id = graphene.Int()
    organization_id = graphene.Int()
    case_id = graphene.Int()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):

        fullname = input.get('fullname') or ''

        split_fullname = fullname.split(' ')
        first_name = clear_name_title(split_fullname[0])
        last_name = ''

        if len(split_fullname) > 1:
            last_name = ' '.join(split_fullname[1:])

        person_id = input.get('person_id') or None
        if person_id:
            person = Person.objects.get(id=person_id)
        else:
            person, created = Person.objects.get_or_create(first_name=first_name, last_name=last_name)

        organization_name = input.get('organization_name') or ''
        organization_type = input.get('organization_type') or Organization.ORGANIZATION_TYPE_CHOICES[0]

        organization_id = input.get('organization_id') or None
        if organization_id:
            organization = Organization.objects.get(id=organization_id)
        else:
            organization, created = Organization.objects.get_or_create(name=organization_name,
                                                                       organization_type=organization_type)

        cause_of_case = input.get('cause_of_case') or ''
        black_case_number = input.get('black_case_number') or ''
        red_case_number = input.get('red_case_number') or ''

        address_province = input.get('case_status') or ''
        address_district = input.get('address_district') or ''
        address_sub_district = input.get('address_sub_district') or ''

        case_status = input.get('case_status') or ''
        case_summary = input.get('case_summary') or ''

        case_id = input.get('case_id') or None
        if case_id:
            case = Case.objects.get(id=case_id)
        else:
            case, created = Case.objects.get_or_create(cause_of_case=cause_of_case,
                                                       black_case_number=black_case_number,
                                                       red_case_number=red_case_number,
                                                       address_province=address_province,
                                                       address_district=address_district,
                                                       address_sub_district=address_sub_district,
                                                       status=case_status,
                                                       summary=case_summary)

        position = input.get('position') or ''
        position_year = input.get('position_year') or ''
        position_start_year = input.get('position_start_year') or ''
        position_end_year = input.get('position_end_year') or ''

        organization_person, created = OrganizationPerson.objects.get_or_create(person=person,
                                                                                organization=organization,
                                                                                position=position,
                                                                                year=position_year,
                                                                                start_year=position_start_year,
                                                                                end_year=position_end_year)

        is_guilty = input.get('is_guilty') or ''
        guilty_level = input.get('guilty_level') or ''
        person_judgement = input.get('person_judgement') or ''
        case_person, created = CasePerson.objects.get_or_create(person=person,
                                                                case=case,
                                                                is_guilty=is_guilty,
                                                                guilty_level=guilty_level,
                                                                person_judgement=person_judgement)

        return cls(success=True,
                   person_id=person.id,
                   organization_id=organization.id,
                   case_id=case.id)


class Mutation(ObjectType):
    event = EventMutation.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
