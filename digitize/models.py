from django.db import models


class Common(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Person(Common):
    prefix_name = models.CharField(verbose_name='คำนำหน้าชื่อ', max_length=1000, null=True, blank=True)
    first_name = models.CharField(verbose_name='ชื่อ', max_length=1000, null=True, blank=True)
    last_name = models.CharField(verbose_name='นามสกุล', max_length=1000, null=True, blank=True)

    full_name = models.CharField(verbose_name='ชื่อเต็ม', max_length=1000, null=True, blank=True)


    def __str__(self):
        return '{}'.format(self.full_name)


class OrganizationManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().exclude(name='').exclude(organization_people__person__first_name='').distinct()

class Organization(Common):
    ORGANIZATION_TYPE_CHOICES = ('หน่วยงานรัฐ', 'บริษัทจำกัด', 'ห้างหุ้นส่วน')
    ORGANIZATION_TYPE_CHOICES = list(zip(ORGANIZATION_TYPE_CHOICES, ORGANIZATION_TYPE_CHOICES))

    name = models.CharField(verbose_name='ชื่อองค์กร', max_length=1000)
    organization_type = models.CharField(verbose_name='ประเภทองค์กร', max_length=1000, null=True, blank=True,
                                         choices=ORGANIZATION_TYPE_CHOICES)
    registration_no = models.CharField(verbose_name='เลขที่จดทะเบียน', max_length=1000, null=True, blank=True)
    objects = OrganizationManager()

    def __str__(self):
        return self.name


class Project(Common):
    name = models.CharField(verbose_name='ชื่อโครงการ', max_length=1000)
    budget = models.DecimalField(verbose_name='งบประมาณโครงการ', max_digits=8, decimal_places=2, null=True, blank=True)

    address_province = models.CharField(verbose_name='พื้นที่ในจังหวัด', max_length=1000, null=True, blank=True)
    address_district = models.CharField(verbose_name='พื้นที่ในอำเภอ', max_length=1000, null=True, blank=True)
    address_sub_district = models.CharField(verbose_name='พื้นที่ในตำบล', max_length=1000, null=True, blank=True)

    quotation_document = models.CharField(verbose_name='ใบเสนอราคา', max_length=1000, null=True, blank=True)
    certification_document = models.CharField(verbose_name='ใบประกาศ', max_length=1000, null=True, blank=True)


    def __str__(self):
        return self.name


class Case(Common):
    source =  models.CharField(verbose_name='แหล่งที่มา', max_length=200, null=True, blank=True, choices=(('nacc', 'ปปช'), ('deka', 'ศาลฎีกา')))
    source_ref_number = models.CharField(verbose_name='เลขอ้างอิง', max_length=200, null=True, blank=True)
    cause_of_case = models.TextField(verbose_name='มูลความผิด')
    case_date = models.DateField(verbose_name='วันที่ประกาศ', null=True, blank=True)

    black_case_number = models.CharField(verbose_name='หมายเลขคดีดำ', max_length=1000, null=True, blank=True)
    red_case_number = models.CharField(verbose_name='หมายเลขคดีแดง', max_length=1000, null=True, blank=True)

    address_province = models.CharField(verbose_name='พื้นที่เกิดเหตุในจังหวัด', max_length=1000, null=True, blank=True)
    address_district = models.CharField(verbose_name='พื้นที่เกิดเหตุในอำเภอ', max_length=1000, null=True, blank=True)
    address_sub_district = models.CharField(verbose_name='พื้นที่เกิดเหตุในตำบล', max_length=1000, null=True, blank=True)

    status = models.TextField(verbose_name='สถานะคดี', null=True, blank=True)
    summary = models.TextField(verbose_name='สรุปผลการตัดสิน', null=True, blank=True)

    def __str__(self):
        return self.cause_of_case


class OrganizationPerson(Common):
    person = models.ForeignKey(Person, verbose_name='เจ้าหน้าที่องค์กร', related_name='person_organizations',
                               on_delete=models.DO_NOTHING)
    organization = models.ForeignKey(Organization, verbose_name='องค์กรในสังกัด', related_name='organization_people',
                                     on_delete=models.DO_NOTHING)
    position = models.TextField(verbose_name='ตำแหน่ง', null=True, blank=True)

    year = models.CharField(verbose_name='ปีที่ดำรงตำแหน่ง', max_length=1000, null=True, blank=True)
    start_year = models.CharField(verbose_name='ปีที่เริ่มดำรงตำแหน่ง', max_length=1000, null=True, blank=True)
    end_year = models.CharField(verbose_name='ปีที่สิ้นสุดดำรงตำแหน่ง', max_length=1000, null=True, blank=True)

    def __str__(self):
        return '{} {} [{}]'.format(self.person, self.organization, self.position)


class OrganizationProject(Common):
    project = models.ForeignKey(Project, verbose_name='องค์กรที่เกี่ยวข้อง', related_name='project_organizations',
                                on_delete=models.DO_NOTHING)
    organization = models.ForeignKey(Organization, verbose_name='โครงการที่เกี่ยวข้อง',
                                     related_name='organization_projects',
                                     on_delete=models.DO_NOTHING)
    status = models.TextField(verbose_name='สถานะที่เกี่ยวข้อง', null=True, blank=True)

    def __str__(self):
        return '{} {}'.format(self.project, self.organization)


class OrganizationCase(Common):
    case = models.ForeignKey(Case, verbose_name='ความผิดที่โดยกล่าวหา', related_name='case_organizations',
                             on_delete=models.DO_NOTHING)
    organization = models.ForeignKey(Organization, verbose_name='ที่เกี่ยวข้อง', related_name='organization_cases',
                                     on_delete=models.DO_NOTHING)

    def __str__(self):
        return '{} {}'.format(self.case, self.organization)


class ProjectPerson(Common):
    person = models.ForeignKey(Person, verbose_name='รับผิิดชอบโครงการ', related_name='person_projects',
                               on_delete=models.DO_NOTHING)
    project = models.ForeignKey(Project, verbose_name='โครงการที่ได้รับมอบหมาย', related_name='project_people',
                                on_delete=models.DO_NOTHING)
    position = models.TextField(verbose_name='หน้าที่รับผิดชอบ', null=True, blank=True)

    def __str__(self):
        return '{} {}'.format(self.person, self.project)


class ProjectCase(Common):
    case = models.ForeignKey(Case, verbose_name='ความผิดที่โดยกล่าวหา', related_name='case_projects',
                             on_delete=models.DO_NOTHING)
    project = models.ForeignKey(Project, verbose_name='โครงการที่เกี่ยวข้อง', related_name='project_cases',
                                on_delete=models.DO_NOTHING)

    def __str__(self):
        return '{} {}'.format(self.case, self.project)


class CasePerson(Common):
    person = models.ForeignKey(Person, verbose_name='บุคคลที่เกี่ยวข้อง', related_name='person_cases',
                               on_delete=models.DO_NOTHING)
    case = models.ForeignKey(Case, verbose_name='ความผิดที่โดยกล่าวหา', related_name='case_people',
                             on_delete=models.DO_NOTHING)

    is_guilty = models.BooleanField(verbose_name='สถานะความผิด', null=True, blank=True)
    guilty_level = models.TextField(verbose_name='ระดับความผิด', null=True, blank=True)
    person_judgement = models.BooleanField(verbose_name='สถานะความผิด', null=True, blank=True)

    def __str__(self):
        return '{} {}'.format(self.person, self.case)
