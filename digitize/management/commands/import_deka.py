import datetime
import re

import requests
from django.core.management.base import BaseCommand, CommandError

import json
import csv

from digitize.models import Case, Person, CasePerson, Organization, OrganizationPerson, OrganizationCase
from digitize.utils import get_names


def get_and_save_actai_company(person, case):
    query = {
        "query": {
            "bool": {
                "must": [{
                    "query_string": {
                        "query": "\"%s %s\"" % (person.first_name, person.last_name),
                        "fields": [
                            "shareHolder", "commitee", "name"
                        ]
                    }
                }]
            }
        }
    }

    response = requests.post('https://act-es2.iapp.co.th/act_company/_search', json=query)
    print('find', person.first_name, person.last_name)
    result = response.json()
    if result['hits']['total'] > 0:
        print(result)

        for hit in result['hits']['hits']:
            company = hit['_source']

            organization_data = {
                'name': company.get('name'),
                'organization_type': company.get('typeOfEntity'),
                'registration_no': company.get('id')
            }

            organization, created = Organization.objects.get_or_create(**organization_data, defaults=organization_data)

            OrganizationCase.objects.get_or_create(organization=organization, case=case, defaults={
                'organization': organization,
                'case': case,
            })

            position = ''
            name = '%s %s' % (person.first_name, person.last_name)
            if company.get('commitee') and company.get('commitee').search(name):
                position = 'committee'
            elif company.get('shareHolder') and company.get('shareHolder').search(name):
                position = 'shareholder'
            else:
                continue

            OrganizationPerson.objects.get_or_create(organization=organization, person=person, defaults={
                'organization': organization,
                'person': person,
                'position': position,
            })

class Command(BaseCommand):
    help = 'Import deka supreme court case from csv'

    def add_arguments(self, parser):
        parser.add_argument('--input')

    def handle(self, *args, **options):
        input_file = options['input']

        if not input_file:
            raise Exception('input file is required')

        with open(input_file, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')

            is_header = True
            header = None
            for row in reader:
                if is_header:
                    header = row
                    is_header = False
                    for idx, col in enumerate(header):
                        print(idx, col)
                    continue

                item = {header[k]: v for k, v in enumerate(row)}

                cause_of_case = item['ประมวลกฏหมายอาญา มาตรา  (กรอกเป็นตัวเลข เช่น 57, 58)'] + ', ' + item['ประมวลกฏหมายอื่น ๆ (เช่น กฎหมายปปช มาตรา 1, 2)']
                case_date_year = re.search(r'\/(\d{4})\s*$', item['เลขคดี'])
                case_date = None
                if case_date_year:
                    year = case_date_year[1]
                    case_date = datetime.date(int(year), 1, 1)

                case_ref = re.search(r'(\d*\/\d{4})\s*$', item['เลขคดี'])
                if case_ref:
                    case_ref = case_ref[0]

                verdict_result = False
                for i in range(1, 6):
                    if item['สถานะจำเลยคนที่ ' + str(i)] == 'มีความผิด':
                        verdict_result = True
                        break

                case_data = {
                    'source': 'deka',
                    'source_ref_number': case_ref,
                    'cause_of_case': cause_of_case,
                    'case_date': case_date,
                    'address_province': item['สถานที่เกิดเหตุ จังหวัด'],
                    'address_district': item['สถานที่เกิดเหตุ อำเภอ'],
                    'address_sub_district': item['สถานที่เกิดเหตุ ตำบล'],
                    'status': verdict_result,
                }
                c, created = Case.objects.get_or_create(**case_data, defaults=case_data)

                for i in range(1, 6):
                    full_name = item['ชื่อจำเลยคนที่ ' + str(i)].replace(' กับพวก', '').strip()
                    if not full_name:
                        continue

                    name = get_names(full_name)
                    person_data = {
                        'first_name': name['first_name'],
                        'last_name': name['last_name'],
                        'full_name': full_name,
                    }
                    p, created = Person.objects.get_or_create(**person_data, defaults=person_data)

                    org_data = {
                        'name': item['องค์กรจำเลยคนที่ ' + str(i)].replace(' กับพวก', '').strip(),
                    }
                    org, created = Organization.objects.get_or_create(defaults=org_data, **org_data)

                    org_case_data = {
                        'case': c,
                        'organization': org,
                    }
                    org_case = OrganizationCase.objects.get_or_create(defaults=org_case_data, **org_case_data)

                    position = item['ตำแหน่งจำเลยคนที่ ' + str(i)]
                    if position:
                        org_person_data = {
                            'person': p,
                            'organization': org,
                            'position': position,
                        }
                        org_person, created = OrganizationPerson.objects.get_or_create(defaults=org_person_data, **org_person_data)

                    get_and_save_actai_company(p, c)

                    guilty_level = [item['ถ้ามีความผิด กรุณากรอกผลคำพิพากษาจำเลยคนที่ ' + str(i)], item['ผลคำพิพากษาลงโทษจำคุกจำเลยคนที่ ' + str(i) + ' (ถ้ามี ให้กรอกจำนวน ปี, เดือน, วัน)']]
                    guilty_level = ', '.join([x for x in guilty_level if x])
                    case_person_data = {
                        'person': p,
                        'case': c,
                        'is_guilty': item['สถานะจำเลยคนที่ ' + str(i)] == 'มีความผิด',
                        'guilty_level': guilty_level,
                        'person_judgement': item['สถานะจำเลยคนที่ ' + str(i)] == 'มีความผิด',
                    }
                    cp, created = CasePerson.objects.get_or_create(defaults=case_person_data, person=p, case=c)
                    if created:
                        cp.guilty_level = guilty_level
                        cp.save()