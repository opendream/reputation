from django.core.management.base import BaseCommand, CommandError
import requests
from digitize.models import Case, CasePerson, Person, Organization, OrganizationCase, OrganizationPerson

import dateutil.parser as DP

import json
import csv
from digitize.utils import get_names


class Command(BaseCommand):
    help = 'Import NACC results as model'

    def handle(self, *args, **options):
        with open('digitize/data/NACC Data - Form Responses 1.csv', mode='r') as infile:
            reader = csv.reader(infile)

            source = 'nacc'

            is_header = True
            header = None
            for row in reader:
                if is_header:
                    header = row
                    is_header = False
                    continue

                item = {header[k]: v for k, v in enumerate(row)}

                try:
                    case_date = (item['วันที่มีมติ (ปี/เดือน/วัน)'] and DP.parse(item['วันที่มีมติ (ปี/เดือน/วัน)'])) or None
                except:
                    case_date = None

                case_data = {
                    'source': source,
                    'case_date': case_date,
                    'black_case_number': item['เลขคดีดำ'],
                    'red_case_number': item['เลขที่คดีแดง'],
                    'address_province': item['สถานที่เกิดเหตุ จังหวัด'],
                    'address_district': item['สถานที่เกิดเหตุ อำเภอ'],
                    'address_sub_district': item['สถานที่เกิดเหตุ ตำบล'],
                }

                case, created = Case.objects.get_or_create(**case_data, defaults=case_data)

                people = []


                for i in range(1, 5):
                    if not item['ชื่อ ผู้ถูกกล่าวหาคนที่ %d' % i]:
                        continue

                    row_name = item['ชื่อ ผู้ถูกกล่าวหาคนที่ %d' % i].replace('หรือ', '|')

                    for name in row_name.split('|'):

                        case_person_data = get_names(name.strip())
                        case_person_data['position'] = item['ตำแหน่ง ผู้ถูกกล่าวหาคนที่ %d' % i]
                        case_person_data['person_judgement'] = 'ไม่' not in item['การตัดสิน ของผู้ถูกกล่าวหาคนที่ %d' % i]
                        people.append(case_person_data)

                for person_data in people:

                    position = person_data.pop('position')
                    person_judgement = person_data.pop('person_judgement')

                    person, created = Person.objects.get_or_create(**person_data, defaults=person_data)
                    CasePerson.objects.get_or_create(person=person, case=case, person_judgement=person_judgement, defaults={'person': person, 'case': case, 'person_judgement': person_judgement})


                    # Join to actai
                    query = {
                        "query": {
                            "bool": {
                                "must": [{
                                    "query_string": {
                                        "query": "\"%s %s\"" % (person.first_name, person.last_name)
                                    }
                                }]
                            }
                        },
                        "aggs": {
                            "statMoney": {
                                "stats": {
                                    "field": "projectMoney"
                                }
                            }
                        }
                    }

                    response = requests.post('https://act-es2.iapp.co.th/act_company/_search', json=query)
                    print('find', person.first_name, person.last_name)
                    result = response.json()
                    if result and result.get('hits') and result['hits']['total'] > 0:
                        print(result)

                        for hit in result['hits']['hits']:
                            company = hit['_source']

                            organization_data = {
                                'name': company.get('name'),
                                'organization_type': company.get('typeOfEntity'),
                                'registration_no': company.get('id')
                            }

                            organization, created = Organization.objects.get_or_create(**organization_data, defaults=organization_data)

                            OrganizationCase.objects.get_or_create(organization=organization, case=case, defaults={
                                'organization': organization,
                                'case': case,
                            })

                            OrganizationPerson.objects.get_or_create(organization=organization, person=person, defaults={
                                'organization': organization,
                                'person': person,
                                'position': position
                            })
