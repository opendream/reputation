from django.core.management.base import BaseCommand, CommandError
from domain.models import Case

import json
import csv


class Command(BaseCommand):
    help = 'Export NACC crawl results as json and csv'

    def add_arguments(self, parser):
        parser.add_argument('--format')
        parser.add_argument('--output')

    def handle(self, *args, **options):
        output_format = options['format']
        if not output_format:
            output_format = 'json'

        if output_format == 'csv':
            self.handle_csv(*args, **options)

        if output_format == 'json':
            self.handle_json(*args, **options)

    def handle_csv(self, *args, **options):
        output_file = options['output']
        with open(output_file, 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', quotechar='\"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow([
                'CaseNo - Undecided', 'CaseNo - Decided',
                'Verdict Order', 'Verdict Date',
                'Defendant',
                'Accusation',
                'NCA Result',
                'Actions Activity', 'Actions Result', 'Actions Remark',
                'Case Updated Date',
            ])

            for case in Case.objects.all():
                writer.writerow([
                    case.case_no_undecided, case.case_no_decided,
                    case.verdict_order, case.verdict_date.strftime('%Y-%m-%d'),
                    case.defendant if case.defendant else case.raw_defendant,
                    case.accusation,
                    case.result,
                    case.action_activity, case.action_result, case.action_remark,
                    case.case_updated_date.strftime('%Y-%m-%d'),
                ])

    def handle_json(self, *args, **options):
        print('[')

        cases = Case.objects.all()
        cases_count = cases.count()
        output_count = 0
        for case in cases:
            data = {
                'caseNo': {
                    'undecided': case.case_no_undecided,
                    'decided': case.case_no_decided,
                },
                'verdict': {
                    'order': case.verdict_order,
                    'date': case.verdict_date.strftime('%Y-%m-%d'),
                },
                'defendant': case.defendant if case.defendant else case.raw_defendant,
                'accusation': case.accusation,
                'NCAresult': case.result,
                'actions': {
                    'activity': case.action_activity,
                    'result': case.action_result,
                    'remark': case.action_remark,
                },
                'caseUpdatedDate': case.case_updated_date.strftime('%Y-%m-%d'),
            }
            print(json.dumps(data), )
            if output_count < cases_count - 1:
                print(',')
            output_count += 1
        print(']')
