from django.contrib.postgres.fields import JSONField
from django.db import models

class Common(models.Model):

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Party(Common):

    PARTY_TYPE_CHOICES = (
        ('person', 'บุคคล'),
        ('organization', 'องค์กร'),
    )

    party_type = models.CharField(verbose_name='ประเภท', max_length=250, choices=PARTY_TYPE_CHOICES)


class Organization(Party):

    ORGANIZATION_TYPE_CHOICES = ('รัฐบาล', 'รัฐวิสาหกิจ', 'เอกชน')
    ORGANIZATION_TYPE_CHOICES = list(zip(ORGANIZATION_TYPE_CHOICES, ORGANIZATION_TYPE_CHOICES))

    name = models.CharField(verbose_name='ชื่อองค์กร', max_length=1000, unique=True)

    organization_type = models.CharField(verbose_name='ประเภทองค์กร', max_length=1000, null=True, blank=True, choices=ORGANIZATION_TYPE_CHOICES)
    registration_no = models.CharField(verbose_name='ชื่อองค์กร', max_length=1000, null=True, blank=True)
    number_of_shares = models.DecimalField(verbose_name='จำนวนหุ้น', max_digits=8, decimal_places=2, null=True, blank=True)

    parent = models.ForeignKey('self', verbose_name='สังกัดองค์กร', related_name='children', null=True, blank=True, on_delete=models.SET_NULL)

    def save(self, *args, **kwargs):
        self.party_type = 'organization'
        super().save(*args, **kwargs)


class Position(Common):

    organization = models.ForeignKey(Organization, verbose_name='องค์กร', related_name='positions', on_delete=models.CASCADE)
    name = models.CharField(verbose_name='ชื่อตำแหน่ง', max_length=1000)

    start_date = models.DateField(verbose_name='รับตำแหน่งเมื่อวันที่', null=True, blank=True)
    end_date = models.DateField(verbose_name='ออกจากตำแหน่งเมื่อวันที่', null=True, blank=True)


class ShareHolder(Common):

    organization = models.ForeignKey(Organization, verbose_name='องค์กร', related_name='share_holders', on_delete=models.CASCADE)
    number_of_shares_held = models.DecimalField(verbose_name='จำนวนหุ้นที่ถือ', max_digits=8, decimal_places=2, null=True, blank=True)

    start_date = models.DateField(verbose_name='เริ่มถือหุ้นเมื่อวันที่', null=True, blank=True)
    end_date = models.DateField(verbose_name='เลิกถือหุ้นเมื่อวันที่', null=True, blank=True)


class Person(Party):

    prefix_name = models.CharField(verbose_name='คำนำหน้าชื่อ', max_length=250, default='')
    first_name = models.CharField(verbose_name='ชื่อ', max_length=250)
    last_name = models.CharField(verbose_name='นามสกุล', max_length=250)

    name = models.CharField(verbose_name='ชื่อ - นามสกุล', max_length=1000)

    positions = models.ManyToManyField(Position, verbose_name='ผู้ดูแลสถานประกอบการ', blank=True)
    share_holders = models.ManyToManyField(ShareHolder, verbose_name='ผู้ดูแลสถานประกอบการ', blank=True)

    class Meta:
        unique_together = ['first_name', 'last_name']

    def save(self, *args, **kwargs):
        self.party_type = 'person'
        self.name = ('%s%s %s' % (self.prefix_name, self.first_name, self.last_name)).strip()
        super().save(*args, **kwargs)


class Case(Common):

    case_no_undecided = models.CharField(verbose_name='เลขดำที่/เลขคดีดำที่', max_length=250, null=True, blank=True)
    case_no_decided = models.CharField(verbose_name='เลขแดงที่/เลขคดีแดงที่', max_length=250, null=True, blank=True)

    verdict_order = models.CharField(verbose_name='ครั้งที่มีมติ', max_length=250, null=True, blank=True)
    verdict_date = models.DateField(verbose_name='วันที่มีมติ', max_length=250, null=True, blank=True)

    defendant = models.ForeignKey(Party, verbose_name='ผู้ถูกกล่าวหา (อ้างอิง)', related_name='defendant_cases',  null=True, blank=True, on_delete=models.SET_NULL)
    raw_defendant = models.TextField(verbose_name='ผู้ถูกกล่าวหา', null=True, blank=True)

    plaintiff = models.ForeignKey(Party, verbose_name='ผู้กล่าวหา (อ้างอิง)', related_name='plaintiff_cases',  null=True, blank=True, on_delete=models.SET_NULL)
    raw_plaintiff = models.TextField(verbose_name='ผู้กล่าวหา', null=True, blank=True)

    accusation = models.TextField(verbose_name='ข้อกล่าวหา', null=True, blank=True)

    result = models.TextField(verbose_name='ผลการพิจารณา', null=True, blank=True, help_text='ผลการพิจารณาของคณะกรรมการป.ป.ช.')

    action_activity = models.TextField(verbose_name='การดำเนินการ', null=True, blank=True)
    action_result = models.TextField(verbose_name='ผลการดำเนินการ', null=True, blank=True)
    action_remark = models.TextField(verbose_name='หมายเหตุ', null=True, blank=True)

    case_updated_date = models.DateField(verbose_name='วันที่ปรับปรุงข้อมูล', null=True, blank=True)