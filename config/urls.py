from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt
# from graphene_file_upload.django import FileUploadGraphQLView
from common.views import CommonGraphQLView
from graphql_jwt.decorators import jwt_cookie
from django.conf.urls.static import static


urlpatterns = [
    path('graphql/', (csrf_exempt(CommonGraphQLView.as_view(graphiql=True)))),
    path('admin/', admin.site.urls),
    path('admin/graphql/', csrf_exempt(CommonGraphQLView.as_view(graphiql=True))),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
