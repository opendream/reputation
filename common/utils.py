import re
from datetime import datetime

trimmer_re = re.compile(r'\s*', re.MULTILINE)


def clean_string(str_list):
    text = ''.join(str_list)
    return trimmer_re.sub(' ', text, -1).strip()


def thai_date_to_date(thai_date):
    month_map = {'มกราคม': 'January', 'กุมภาพันธ์': 'February', 'มีนาคม': 'March', 'เมษายน': 'April', 'พฤษภาคม': 'May', 'มิถุนายน': 'June', 'กรกฎาคม': 'July', 'สิงหาคม': 'August', 'กันยายน': 'September', 'ตุลาคม': 'October', 'พฤศจิกายน': 'November', 'ธันวาคม': 'December'}
    date = thai_date.split(' ')
    date[1] = month_map[date[1]]
    date[2] = str(int(date[2]) - 543)

    date = ' '.join(date)

    return datetime.strptime(date, '%d %B %Y').date()


def get_row_text(row, no, clean=False):
    text = row.css('td:nth-child(%d) *::text' % (no,)).getall()
    if clean:
        return clean_string(text)

    return text


def clear_name_title(name):
    NAME_TITLES = (
        'นางสาว',
        'นส.',
        'น.ส.'
        'นาง',
        'นาย',

    )

    for pattern in NAME_TITLES:
        name = re.sub(pattern, '', name)
    return name
