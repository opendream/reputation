import json
import urllib
from datetime import datetime
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from graphene.utils.str_converters import to_camel_case
from graphene_file_upload.django import FileUploadGraphQLView
from graphql_relay import to_global_id, from_global_id
from graphql_jwt.settings import jwt_settings
from graphql_jwt.utils import get_credentials, get_payload

import dateutil.parser as DP

import pytz

utc = pytz.UTC


@login_required
def home(request):
    return render(request, 'home.html')


def to_dt(value):

    return value and DP.parse(value)


def to_d(value):
    return value and datetime.strptime(value, '%Y-%m-%d').date()


class CommonGraphQLView(FileUploadGraphQLView):

    def get_response(self, request, data, show_graphiql=False):
        query, variables, operation_name, id = self.get_graphql_params(request, data)
        response = super().get_response(request, data, show_graphiql)
        return response
