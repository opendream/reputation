# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import os
import pathlib
from os import mkdir
from shutil import copyfile

from domain.models import Case
from scrapy.pipelines.files import FilesPipeline



# class ReputationPipeline(object):
#     def process_item(self, data, spider):
#         case = Case(**data)
#         case.save()
#         return data

class NACCDeclarationPipeline(object):
    def process_item(self, data, spider):
        start_doc_file_path = data['files'][0]['path']
        end_doc_file_path = data['files'][0]['path']
        if len(data['files']) > 1:
            end_doc_file_path = data['files'][1]['path']

        if spider.name == 'nacc_declarations_mp':
            file_path = os.path.join('./files', data['files'][0]['path'])
            if file_path:
                dest_dir = os.path.join('./files/nacc/declarations/MPs/', data['no'] + '_' + data['name'])
                pathlib.Path(dest_dir).mkdir(parents=True, exist_ok=True)
                dest = os.path.join(dest_dir, 'start_doc.pdf')
                copyfile(file_path, dest)
                start_doc_file_path = dest

        if spider.name == 'nacc_declarations_senator':
            file_path = os.path.join('./files', data['files'][0]['path'])
            if file_path:
                dest_dir = os.path.join('./files/nacc/declarations/Senators/', data['no'] + '_' + data['name'])
                pathlib.Path(dest_dir).mkdir(parents=True, exist_ok=True)
                dest = os.path.join(dest_dir, 'start_doc.pdf')
                copyfile(file_path, dest)
                start_doc_file_path = dest

        if spider.name == 'nacc_declarations_legislator':
            file_path = os.path.join('./files', end_doc_file_path)
            if file_path:
                dest_dir = os.path.join('./files/nacc/declarations/Legislators/', data['no'] + '_' + data['name'])
                pathlib.Path(dest_dir).mkdir(parents=True, exist_ok=True)
                dest = os.path.join(dest_dir, 'end_doc.pdf')
                copyfile(file_path, dest)
                end_doc_file_path = dest

        data['start_doc_file_path'] = start_doc_file_path
        data['end_doc_file_path'] = end_doc_file_path
        return data