import scrapy

from common.utils import clean_string


class PositionSpider(scrapy.Spider):
    name = 'sec_person_position_spider'
    start_urls = ['https://capital.sec.or.th/ssl_client/eds/eds_resultpos2.php']

    current_page = 1
    page_limit = 0

    def parse(self, response):
        for row in response.css('table:nth-child(2) tr'):
            yield {
                'name': clean_string(row.css('td:nth-child(2) *::text').getall()),
                'company': clean_string(row.css('td:nth-child(3) *::text').getall()),
                'position': clean_string(row.css('td:nth-child(4) *::text').getall()),
                'meta': {
                    'training_history': clean_string(row.css('td:nth-child(5) *::text').getall()),
                    'row_order': clean_string(row.css('td:nth-child(1) *::text').getall()),
                }
            }

        links = response.css('p > a.linkb')
        if self.page_limit <= 0 or (self.current_page < self.page_limit and len(links) >= self.current_page):
            next_page = links[self.current_page].attrib['href']
            self.current_page += 1
            yield response.follow(next_page, self.parse)
