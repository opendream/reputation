
import scrapy
from scrapy import Request

from common.utils import thai_date_to_date, clean_string, get_row_text

class NccCaseSettledSpider(scrapy.Spider):

    name = "ncc_case_settled"
    start_urls = ['https://www.nacc.go.th/culpability.php']

    def parse_result(self, response):
        text = clean_string(response.xpath('//table//table//tr[5]/td[2]').css('*::text').getall())
        data = response.meta['data']
        data['result'] = text
        yield data


    def parse(self, response):
        rows = response.css('table[div] tr')
        rows.pop(0)

        for row in rows:
            data = {
                'case_no_undecided': get_row_text(row, 1, True),
                'case_no_decided': get_row_text(row, 2, True),
                'verdict_order': get_row_text(row, 3, True),
                'verdict_date': thai_date_to_date(get_row_text(row, 4, True)),
                'raw_defendant': get_row_text(row, 5, True),
                'accusation': get_row_text(row, 6, True),
                'result': '',
                'action_activity': get_row_text(row, 8, True),
                'action_result': get_row_text(row, 9, True),
                'action_remark': get_row_text(row, 10, True),
                'case_updated_date': thai_date_to_date(get_row_text(row, 11, True)),
                'raw_plaintiff': 'ป.ป.ช.'
            }
            result_url = clean_string(row.xpath('.//td[7]//a/@href').extract())
            result_url = 'https://www.nacc.go.th' + result_url
            yield Request(result_url, callback=self.parse_result, meta={'data': data})

        next_page = response.css('a.paginate:last-child::attr(href)').get()
        if next_page and next_page != '#':
            yield response.follow(next_page, self.parse)
