import scrapy

from common.utils import clean_string


def get_item_lists(row, selector):
    items = []
    for item in row.css(selector):
        items.append(clean_string(item.css('::text').getall()))

    return items


class PositionSpider(scrapy.Spider):
    name = 'deka_search'
    start_urls = ['http://deka.supremecourt.or.th/search']

    current_page = 1
    page_limit = 0

    def parse(self, response):
        for row in response.css('#deka_result_info li.result'):
            sources = get_item_lists(row, 'li.item_source li')
            laws = get_item_lists(row, 'li.item_law li')
            ligitants = get_item_lists(row, 'li.item_ligitant li')
            judges = get_item_lists(row, 'li.item_judge li')
            courts = get_item_lists(row, 'li.item_primarycourt li')

            yield {
                'title': clean_string([row.css('.item_deka_no input[type=hidden]').attrib['value']]),
                'details': get_item_lists(row, '.item_short_text p.content-detail'),
                'laws': laws,
                'sources': sources,
                'ligitants': ligitants,
                'judges': judges,
                'courts': courts,
            }

        links = response.css('ul.pagination > li a[href="http://deka.supremecourt.or.th/search/index/%d"]' % (self.current_page + 1, ))
        if self.page_limit <= 0 or (self.current_page < self.page_limit and len(links) > 0):
            next_page = links[0].attrib['href']
            self.current_page += 1
            yield response.follow(next_page, self.parse)
