import scrapy
import datetime
import re

from common.utils import clean_string

date_pattern = '%d/%m/%Y'
be_year_re = re.compile(r'(\d{2})/(\d{2})/(\d{4})')


def parse_date(date_str):
    match = be_year_re.findall(date_str)[0]
    return '%d-%02d-%02dT00:00:00+0700' % (int(match[2]) - 543, int(match[1]), int(match[0]))


class PositionSpider(scrapy.Spider):
    name = 'sec_enforce_spider'
    start_urls = ['https://market.sec.or.th/public/idisc/th/ViewMore/enforce-det?QueryType=ALL&OffenderFlag=Y&OffenderTxt=&VioTypeTxt=ALL']

    def parse(self, response):
        for row in response.css('#ctl00_CPH_pnlControl table tr:nth-child(n+2)'):
            news = []
            td_news = clean_string(row.css('td:nth-child(6) *::text').getall())
            if td_news:
                for link in row.css('td:nth-child(6) a'):
                    news.append({
                        'name': clean_string([link.css('::text').get()]),
                        'url': link.attrib['href'],
                    })

            yield {
                'operated_date': parse_date(clean_string(row.css('td:nth-child(2) *::text').getall())),
                'defendant': clean_string(row.css('td:nth-child(3) *::text').getall()),
                'offense': clean_string(row.css('td:nth-child(4) *::text').getall()),
                'brief_action': clean_string(row.css('td:nth-child(5) *::text').getall()),
                'news': news,
                'action': clean_string(row.css('td:nth-child(7) *::text').getall()),
                'detail_action': clean_string(row.css('td:nth-child(8) *::text').getall()),
                'note': clean_string(row.css('td:nth-child(9) *::text').getall()),
                'meta': {
                    'row_order': clean_string(row.css('td:nth-child(1) *::text').getall()),
                }
            }
