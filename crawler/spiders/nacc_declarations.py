
import scrapy

from common.utils import clean_string, get_row_text

file_url_prefix = 'https://asset.nacc.go.th'


class DocItem(scrapy.Item):
    no = scrapy.Field()
    name = scrapy.Field()
    position = scrapy.Field()
    start_date = scrapy.Field()
    end_date = scrapy.Field()
    result = scrapy.Field()
    start_doc_url = scrapy.Field()
    start_doc_file_path = scrapy.Field()
    end_doc_url = scrapy.Field()
    end_doc_file_path = scrapy.Field()

    file_urls = scrapy.Field()
    files = scrapy.Field()

class NccDeclarationsMPSpider(scrapy.Spider):
    name = "nacc_declarations_mp"
    start_urls = ['https://asset.nacc.go.th/dcs-app/disclosure.jsf?reportGroupId=40000&idArray=%2F40000&nameArray=%2F%E0%B8%AA%E0%B8%A1%E0%B8%B2%E0%B8%8A%E0%B8%B4%E0%B8%81%E0%B8%AA%E0%B8%A0%E0%B8%B2%E0%B8%9C%E0%B8%B9%E0%B9%89%E0%B9%81%E0%B8%97%E0%B8%99%E0%B8%A3%E0%B8%B2%E0%B8%A9%E0%B8%8E%E0%B8%A3']

    def parse(self, response):
        rows = response.css('div.panel-body table.table tbody:nth-child(2) tr')

        for row in rows:
            start_date = get_row_text(row, 4, True)
            end_date = get_row_text(row, 5, True)

            start_doc_url = ''
            if start_date != '':
                start_doc_url = clean_string(row.xpath('.//td[4]//a/@href').extract())

            end_doc_url = ''
            if end_date != '':
                end_doc_url = clean_string(row.xpath('.//td[5]//a/@href').extract())

            if start_date.strip() != '' and start_doc_url.strip() != '':
                start_doc_url = file_url_prefix + start_doc_url
            if end_date.strip() != '' and end_doc_url.strip() != '':
                end_doc_url = file_url_prefix + end_doc_url

            doc = DocItem({
                'no': get_row_text(row, 1, True),
                'name': get_row_text(row, 2, True),
                'position': get_row_text(row, 3, True),
                'start_date': start_date,
                'end_date': end_date,
                'result': get_row_text(row, 6, True),
                'start_doc_url': start_doc_url,
                'end_doc_url': end_doc_url,
            })

            doc['file_urls'] = []
            if start_doc_url:
                doc['file_urls'].append(start_doc_url)
            if end_doc_url:
                doc['file_urls'].append(end_doc_url)

            yield doc

        next_page = clean_string(response.css('ul.pagination > li:last-child > a').xpath('./@href').extract())
        yield response.follow(next_page, self.parse)

class NccDeclarationsSenatorSpider(NccDeclarationsMPSpider):
    name = 'nacc_declarations_senator'
    start_urls = ['https://asset.nacc.go.th/dcs-app/disclosure.jsf?reportGroupId=50000&idArray=%2F50000&nameArray=%2F%E0%B8%AA%E0%B8%A1%E0%B8%B2%E0%B8%8A%E0%B8%B4%E0%B8%81%E0%B8%A7%E0%B8%B8%E0%B8%92%E0%B8%B4%E0%B8%AA%E0%B8%A0%E0%B8%B2']

class NccDeclarationsLegislator(NccDeclarationsMPSpider):
    name = 'nacc_declarations_legislator'
    start_urls = ['https://asset.nacc.go.th/dcs-app/disclosure.jsf?reportGroupId=10000&idArray=%2F10000&nameArray=%2F%E0%B8%AA%E0%B8%A1%E0%B8%B2%E0%B8%8A%E0%B8%B4%E0%B8%81%E0%B8%AA%E0%B8%A0%E0%B8%B2%E0%B8%99%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%9A%E0%B8%B1%E0%B8%8D%E0%B8%8D%E0%B8%B1%E0%B8%95%E0%B8%B4%E0%B9%81%E0%B8%AB%E0%B9%88%E0%B8%87%E0%B8%8A%E0%B8%B2%E0%B8%95%E0%B8%B4&pageNo=1']